package io.cl.classroom.classroomgrpcserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassroomGrpcServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClassroomGrpcServerApplication.class, args);
	}

}
