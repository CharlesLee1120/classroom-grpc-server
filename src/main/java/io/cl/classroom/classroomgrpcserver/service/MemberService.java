package io.cl.classroom.classroomgrpcserver.service;

import io.cl.classroom.classroomgrpcserver.grpc.FindMemberServiceGrpc;
import io.cl.classroom.classroomgrpcserver.grpc.Member;
import io.cl.classroom.classroomgrpcserver.grpc.SearchRequest;
import io.cl.classroom.classroomgrpcserver.grpc.SearchResponse;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

@GRpcService
public class MemberService extends FindMemberServiceGrpc.FindMemberServiceImplBase {

    private static final Logger logger = LoggerFactory.getLogger(MemberService.class);

    @Override
    public void search(SearchRequest request, StreamObserver<SearchResponse> responseObserver) {

        logger.info("开始查询用户信息,根据ID: {}", request.getId());

        // 模拟数据源
        Map<String, Member> memberMap = new HashMap();
        memberMap.put("100", Member.newBuilder().setUsername("Odin").setNickname("奥丁").build());
        memberMap.put("200", Member.newBuilder().setUsername("Zeus").setNickname("宙斯").build());
        memberMap.put("300", Member.newBuilder().setUsername("Poseidon").setNickname("波塞冬").build());

        logger.info("开始查询用户信息数据源,根据ID: {}", request.getId());
        Member member = memberMap.get(request.getId());

        logger.info("结束查询用户信息数据源,返回数据: {}", member);

        final SearchResponse response = SearchResponse.newBuilder().setMember(member).build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();

        logger.info("结束查询用户信息, 根据ID: {}", request.getId());
    }
}
