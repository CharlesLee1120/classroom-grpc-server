package io.cl.classroom.classroomgrpcserver.service;

import io.cl.classroom.classroomgrpcserver.grpc.FindMemberServiceGrpc;
import io.cl.classroom.classroomgrpcserver.grpc.Member;
import io.cl.classroom.classroomgrpcserver.grpc.SearchRequest;
import io.cl.classroom.classroomgrpcserver.grpc.SearchResponse;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.junit.jupiter.api.Test;
import org.lognet.springboot.grpc.context.LocalRunningGrpcPort;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class MemberServiceTest {

    @LocalRunningGrpcPort
    int port;

    @Test
    void searchTest() {
        final ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", port).usePlaintext().build();
        final SearchResponse response = FindMemberServiceGrpc.newBlockingStub(channel).search(SearchRequest.newBuilder().setId("200").build());

        Member member = response.getMember();

        assertThat(member.getUsername()).isEqualTo("Zeus");
    }

}
